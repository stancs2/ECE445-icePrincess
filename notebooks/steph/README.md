# Steph's ECE445 Notebook
 - [Development](#development) 
 - [Parts List](#parts-list)
 - [Debugging](#debugging)

# Development
 ## 2/7/2022
 This week we created a preliminary proposal for TA meeting and began thinking about parts and design.
 ## 2/14/2022
 We began creating a parts list and doing research on other PCB designs. We are looking to use the datasheets for IMU boards available for purchase and integrating this into our PCB design. We looked into microcontrollers and decided preliminarily to use the WROOM as its communication protocols align with our design. We were informed that we are unable to use members of the skating team to test our project, so we redefined the moves that we are going to be testing for ease of access to testing data.
 ## 2/21/2022
 This week we had our first design meeting with a professor for the course. We went though our high level requirements and were told to quantify them. Our block diagram needs to expand to account for multiple IMU nodes but good reviews otherwise. We also need to add to OpenCV accuracy calculations to our tolerance analysis. In terms of software we found good material on gitHub and some other codebase platforms. We began researching cameras as we found that some of the material for modeling the human in our software requires a depth camera to extrapolate accurate data.
 ## 2/28/2022
 This week we took feedback from the professors and TAs and have worked on our PCB. We defined a parts list and are attempting to purchase in the near future in order to ensure that we are able to get parts on time. Lionel is going to reach back out to GermanyMan to obtain feedback and software to continue our development of our program. We also obtained 3D printer filament and are working to create a CAD file for the cases for the wearables to match the size of the PCB that we will be mounting inside. We have access to GoPro cameras and are looking to define which one we will be using. The professor that we got our design reviewed by brought up the point that if we are able to define motion through still images we can also go that route if video footage is too buggy to work with in our program.
## 3/7/2022
This week we worked largely on the PCB and parts ordering. I clarified my description of the design in order to help Ethan's progress towards completing the PCB design, and also had some suggestions such as adding ports for microcontroller programming. We also finalized our parts list for ordering.
## 3/14/2022
Spring Break!
## 3/21/2022
This week we ran into a bit of an issue with our parts order. The IMU we had initially selected was no longer available and finding a replacement has proved to be more difficult than we anticipated. We are also beginning to decide the breakdown of what software to work on between Lionel and myself.
## 3/28/2022
This week we have made progress towards our replacement IMU and are now ready to order that part. We have also decided which open source software libraries we will be pulling from to create the simulation of the skater. We will be using OpenCV for object tracking, OpenPose for modeling, and OpenSim for simulation. I have decided to begin the OpenCV portion of the software while Lionel takes on OpenPose. We also have to implement sensor fusion with our new IMU so we will need to find libraries for that as well.
## 4/4/2022
This week I worked to implement OpenCV but encountered issues as descibed in the debugging section below. My next step in debugging this is to test already-existing code. I will also change cameras to a webcam to see if that makes any difference in the functionality. I will continue work on this until I determine it is unfeasible to continue on this path.
## 4/11/2022
This week I continued work on implementing OpenCV but continued to encounter issues with the camera. None of the cameras that I have access to are able to functionally implement OpenCV so awaiting the arrival of the Kinect seems to be the only option (as described in the debugging section below).
## 4/18/2022
The mock demo is this week and the Kinect finally arrived. I came across some research from circa 2014 which is the only resource still available that is compatible with a Kinect this outdated. I am trying to leverage this software as a base for OpenCV or another object tracking library in order to complete our high level requirements.
## 4/25/2022
The final demo is this week and I have broken and fixed the Kinect software that I came across through research from another univeristy several times in several failed attempts to add object tracking to this software. The only thing we can reliably demo is the bare-bones real-time skeletal outline of the skater as they perform the motions. The depth view is much more accurate than the textured view of the skater. I don't know if I will use a depth camera going forward with this project after college. The Kinect, although the best option available to us at this time and also the cheapest possible option for a depth camera in the current market, is very limiting. You can only use a Windows machine to run the Kinect which limits the reach of this implementation; if there are only Linux, MacOS, or alternative OS computers available in the area, it would not be possible to run this software at all. That is not something I would want in the final iteration of this product.
## 5/2/2022
The final presentation is this week and I have tried literally everything I can think of in order to make additions to this research and I cannot get anything to work with this archived version of JDK. I came across some OpenGL research and when I continue this project after graduation, I will use OpenGL to create the wireframe comparison. It is much more up-to-date with current hardware I think as an alpha version of the layered comparison, I will create a "perfect" model and use it as the backdrop video for the user's attempt model.

# Parts List
 - BNO055 Sensor - x1 for each sensor unit
 - microcontroller (ESP 32 WROOM (?)) - for main control unit
 - ribbon cables
 - 2S LiPo Battery (7.4 V)
 - SD breakout board (micro vs. normal)
 - camera (GoPro, 30 FPS -or- Intel Depth Camera)
 - RTC (Real-Time Clock Chip, i2c connection)
 - resistors
 - capacitors
 - voltage regulator
 - Thermoplastic Polyurethane (TPU) -> 3D printing casings
 - mounting screws
 - LEDs

# Debugging
Issue 0: The camera (GoPro-ish thing) we intended to use does not interface with OpenCV at all.

  Issue 1: OpenCV does not interface with the Logitech c920 without issues. Found a github and attempting to resolve.

  Issue 2: Camera interface will not work with any version of Java, Anaconda, or C-based repos that claim to hold solutions to our issues. I called my brother and had him send me the Xbox Kinect depth camera. Awaiting delivery.

  Issue 3: The code repo found to work with the webcam that we are having issues with does not compile, nor does it respond to any changes I make to it. We are not making any progress so awaiting the delivery of the Xbox Kinect seems as though it is our only option to be able to track objects.

  Issue 4: Xbox Kinect depth camera has extremely outdated fitrmware and software assocaited with it, considering its initial release was circa 2013. Although this technology is extremely outdated, the depth camera is very accurate. Resolution involves uninstalling my current JDK and installing a much older, archived version to match that of the Kinect.

  Issue 5: The code repo found to enhance the Xbox Kinect's video output cannot be edited in current versions of any Java-based IDE as they do not process using the archived JDK. I will do what I can to edit and further the software with zero ability to test in an IDE. 
