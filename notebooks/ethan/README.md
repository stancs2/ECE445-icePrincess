# Ethan's ECE 445 Notebook

# Development

## 2/7

### Introduction to Project

###### Objectives:
- Introduction to team
- Understand goals and objectives of project
- Get acquainted with new teammates

After being invited to this project by Steph and Lionel, I was caught up on the concepts and ideas that went into the basic structure of this project. The project is a Digital Coach for Figure Skating, which holds the purpose of being a cost-effective alternative to current methods. Essentially, the user would strap on a set of IMU sensors while being video-recorded with a camera, to track their position and movement while they skate. Their movement would then be compared to an ideal model of certain moves, receiving feedback on what to improve or change in their form, posture, and movement. We also set up and updated our Github page with our proposal and notebooks.

## 2/9
 
### Discussing Design

###### Objectives:
- Begin talking physical/hardware design
- Determine suitable design possibilities
- Understand possible points of weakness and solutions
 
For this meeting, we met up to go over possible designs for an IMU sensor. This pertains to many aspects, such as casing material, necessary interface protocols, and a realistic idea of what our design would look like in action. One of the main components is the BNO055 sensor, which contains an accelerometer, gyroscope, magnetic sensor, and microcontroller, which would be very handy in acquiring positional and rotational data for our project. Our IMU sensors would each be encased in some sort of fall-resistant material, and then each one linked by ribbon cable to the main central unit, which would most likely be held around the torso area. We also went over the basic components for our block diagram. As a basis to our actual project, we were tidying up and editing our proposal to submit to the ECE 445 web board.

![](BNO055.png)
##### BNO055 Sensor Chip

## 2/14

### Design Document

###### Objectives:
- Determine our microcontroller
- Start finding specific parts

After submitting our proposal, our next objective was to compose our design by specifying parts and understanding how we would integrate each part into the whole. Though not official, we are currently looking at the ESP 32 WROOM module to be our microcontroller. To see what else was available, we also took a brief look to search the ECE parts available to us and see if there was anything else that we might need to build the electrical/physical model of our design. I also took the additional time to look into the schematics and design of certain parts to see how compatible they would be, as well as how easy it would be to interface different elements. We came up with a parts list, which is subject to change as we continue to conceptualize the electrical design of our model, but currently contains most of the main basic elements.

### Parts List

- BNO055 Sensor - x1 for each sensor unit, x6 total currently)
- microcontroller (ESP 32 WROOM) - for main central unit
- ribbon cables
- 2S LiPo Battery (7.4 V)
- MicroSD breakout board
- camera (GoPro, 30 FPS)
- RTC (DS1307 - Real-Time Clock Chip, i2c connection)
- resistors
- capacitors
- voltage regulator (LM1117)
- Thermoplastic Polyurethane (TPU) -> 3D printing casings
- mounting screws
- LEDs

## 2/16

### Putting the Parts Together

###### Objectives:
- Determine optimal components (Electrically)
- Split design work into parts (PCB, casing, software)
- Find a camera capable of integrating our software

We looked over the parts, their availability, and costs, to get a general sense of what we were going to use to construct our project. This included looking at the parts shop and list for available parts to see what we find already available instead of waiting for shipping and extra costs. I briefly started designing the electrical circuit schematic, looking at how each individual chip and processor unit is powered and what interfaces it uses to identify the necessary buses. We have to design 2 different sets of schematics: one for the IMU sensors that the user will wear around their body, and the main central unit that collects all the data from the individual sensors. During the meeting, we also discussed how to improve the camera representation in the project so that we were optimizing and utilizing both the camera and the IMUs for complete tracking.

# Designing

## 2/21

### Design Document Check

###### Objectives:
- Present our idea to Professor
- Receive and ask for critical feedback based on the presentation and concepts of our project

Today, we had our design document check with Prof. Schuh. After presenting our project idea and designs with him, we received some feedback to improve our design document, as well as our sophistication and specifications of the different aspects of our project. This included quantifying some of our high level requirements, reevaluating one of our verification tables, updating our block diagram to be more accurate with our physical design, and completing a tolerance analysis based on the tracking aspect of our project. Overall, we found the Q&A session and the review to be very helpful as it guided us to focus more on individual portions of our project and how to make our purpose and goals clear. Many of the adjustments we made helped us pave a better direction of our project and what we needed to focus on to verify and test it.

## 2/23

### Finalizing Design Document

###### Objectives:
- Revise and edit Design Document
- Create a quantifiable tolerance analysis related to the main aspect of our project

At this meeting, we discussed the different points of revision that were mentioned at our design document check. We mainly spent this time going over those points and setting either reasonable changes or quantifiable tolerances that define the accuracy of our scope. We went back and redid some of the R&V tables to make them more realistic in terms of each subsystem's performance. I was able to define a tolerance analysis based on tracking, by analyzing the camera's vision, tolerance rates of the sensor. This tolerance analysis is basically a limit test for the accuracy of camera tracking, and how far off it can be in terms of pixels. It takes into account the different viewing angles of the camera, and the minimum to maximum distance of the skater from the camera for different cases, providing a range of pixels for how well the software should be able to track the IMU nodes in respect to the skater's distance from the camera.

## 2/28

### Design Review

###### Objectives:
- Show that we are experts and know our project well
- Find a new, compatible IMU sensor

The design review went mostly well. The professor had a few questions based on the implementation of both our IMU and camera data aggregation. As the camera should be able to do most of the work, using the IMU sensor data is optimal if we were to implement other moves besides the Bielmann into our program (which would be a further design implementation after our set of data for our first move is calculated). The main purpose of the camera is more to show relative motion from a fixed perspective, while the IMU nodes and hardware design should be able help accurately define the 3D movements of the skater while they move around on the ice. Even if the camera is able to project a wireframe projection of the skater, it only creates a 2D representation, which is not what we are aiming for.

In terms of other progression, the PCB had to be redesigned to fix the control unit. The IMU sensor also had to be remodeled as the BNO055 is out of stock, so it had to be switched to the BHI260AB. This is unfortunate because we changed from a 9-DOF sensor to a 6-DOF sensor, losing the magnetometer measurements that the BNO055 was able to perform. Realistically, we should still be able to use it in our data measurements and implement it into our design. Luckily, the BHI260AB sensor still uses data fusion, so we won't have to manually aggregate our data if there is a spike or outlier.

![](BHI260AB.png)
##### BHI260AB Sensor Chip

## 3/2

### Final Document Overview

###### Objectives:
- Make any last edits to Design Document
- Publish Design Document to ECE 445 webpage

This meeting was pretty basic. We went over the feedback that we heard during our presentation for our final document review, and revised and discussed all points so that the finalized form would be clear and specific in letting other people understand our project and its goals. After working on the changes that were suggested to us, we used the rest of the time to talk over personal progress, and how we could help each other meet our individual goals, in order to reach our team goals overall. For me, I requested help gathering the resources for hardware assembly, such as ribbon cables, battery port converters, and a wire crimping set.

## 3/8

I am personally working on the IMU sensor PCB. This unit is hard to work with because there are 2 layers of traces on the IMU chip that do not allow the smallest track size to get through on KiCad. I had to use some traces that lead to the other side through vias to counteract this, and pessimistically, I do not know if this design will work completely. I am having trouble seeing if there is an easier way to design this, or if I need to somehow remove certain solder pads so that the boundaries are not interfering with each other. I sent an email to David Null to ask for any assistance with this strange situation, but I was unfortunately not able to receive any tips or suggestions that progressed the design of the PCB. I received some ideas about using stencils or looking for a different footprint of the BHI260AB, but I was neither able to find a different footprint or understand exactly what was meant by "using stencils".

## 3/9

### Checkpoints

###### Objectives:
- Debrief and reassess goals
- Set up deadlines moving forward

This meeting was basically just another brief session to understand our goals and current progress. We each filled out the team progress reports, and then discussed what our plan was for when we got back from break. We were able to reestablish deadlines and new goals based on our setbacks and delays. If everything goes as expected, we will receive our new set of PCBs by the time we get back from break. We also still need to order parts, individually from the PCB, such as regulators, SMD components, and the other electrical components for the PCB construction. We are expecting to start hardware construction within the week of getting back, assuming everything is ordered, arrived, and in our possession. 

## 3/21

### Parts Confirmation

###### Objectives:
- Order parts
- Reduce overall cost, if possible

This was just a meeting between Lionel and I, where we confirmed the parts necessary to order for our hardware design, as well as change other components based on the availability and delivery of the current ones in our schematic. This included changing voltage regulators, finding the right component designs for soldering, and putting in our parts order. We spent some time going over several models of different components to see what we could use for our hardware. Even though our PCB technically designates specific footprints, several models look similar and we were trying to be cost-effective as well as find suitable parts that still met our needs since the PCB will be hidden from view once fully assembled and connected. Though we have a budget, electrical component costs can vary quite a lot, so we are being careful with respect to how we use our money.

### Updated Parts List

- BHI260AB Sensor (x6)
- ESP32 WROOM 32 (x1)
- Ribbon Cables (x1)
- 2S LiPo Battery (7.4 V) (x1)
- MicroSD Slot (x1)
- GoPro (x1)
- DS1307 (x1)
- Voltage Regulators (LM1117-1.8, LM1117-3.3) (x1 of each)
- Thermoplastic Polyurethane (TPU) (x1)
- Mounting Screws (x28)

## 3/23

### Setback Again

###### Objectives:
- Visit ECEB Supply Center
- Review control unit and list changes to account for new IMU sensor

During this meeting, we met up at ECEB to pick up our PCB and any electrical parts we could use from the Electronics Service Shop. We were hit by another major setback as we had to change the IMU sensor for the 2nd time: the distributor canceled our order after it was put in and labeled our second choice of sensors as "Out of Stock". We plan to redesign the IMU sensor once again from using the BHI260AB to a new one, which is unfortunate as the other options are older models or do not have data fusion. This means we'll have to make a few adjustments in the code to account for the data inaccuracies. It is very frustrating, but we are trying to keep moving forward. Since we need to change the control unit to account for a different IMU sensor later on, we can do some small voltage and continuity tests on our board, but it would be best not to test too much as our resources for the hardware can be costly. In full, we were only able to submit our control unit design for the PCBway order, and even then, because we changed the IMU sensor, this version of the control unit will not be used in the final design. We also need to order resistors and capacitors. We thought the supply center might have had SMD components, but they did not, so we are ordering them separately.

### SMD Components
Resistors:
- 4.7kΩ (x10)
- 10kΩ (x51)
Capacitors
- 0.1 uF (x14)
- 10 uF (x2)
Diodes
- 1N4148 (x6)
- MBR0520 (x1)
- LED (x7)
N-Channel Gates
- BSS138 (x12)

## 3/28

### New IMU Sensor

###### Objectives:
- Decide on IMU sensor
- Order sensor ASAP
- Rundown any changes necessary to integrate new sensor

In this meeting, we determined our new sensor as the ISM330DHCXTR, which seems to be perfectly fine in terms of stock and availability. It unfortunately does not integrate data fusion, so we will have to fix our own data as it runs, but it is only a minor issue. Steph and Lionel also talked about the code, and how exactly we will use and aggregate our data. This led down to which filters to use to track the IMU nodes on camera, as well as how the IMU sensor will decipher its output data in combination with the camera data. This is another setback, and I will have to redesign the PCB to account for the change of the IMU sensor. This means slightly changing the control unit schematic to have the necessary outputs for the ISM330DHCXTR, while completely scrapping the old IMU PCB unit and starting from scratch.

For me, I have to redesign the PCBs to integrate the ISM330DHCXTR, and reestablish what interfaces and ports need to be adjusted between the two. This is the third time I am designing the PCB for the IMU unit, and it has been really frustrating needing to switch so much, but there honestly is not much we could have done about it as we cannot determine when a company or distributor is going to run out of stock. Today, I am looking at the datasheet for the ISM330DHCXTR and seeing how many ports are necessary to interface the whole design. Since we are able to interface the IMU sensor with SPI, I can use the same wires as inputs for each of the IMUs, with a single wire from each individual node that feeds back into the control unit. The current goal and deadline right now is to finish changing up the design in time for the second round of PCBway orders from the class.

![](ISM330DHCXTR.png)
##### ISM330DHCXTR Sensor Chip

![](controlunitpcb.png)
##### Finalized Control Unit PCB

![](imupcb.png)
##### Finalized IMU PCB

## 3/30

Today, we didn't meet, so we each just worked on the individual progress report. There wasn't too much to discuss as we are currently just in the waiting process for our orders and parts to come in. The most we can work on is the software side, which I know much less about as I am concentrated on hardware design and assembly.

## 4/4

We got the notification that our ordered parts were delivered. We still need the PCB boards to arrive so that we can start working on assembly.

## 4/13

The second wave of PCB orders came in, and we can start assembly soon. I still need the SMD components to arrive as they were ordered separately. I don't like waiting, but currently there isn't too much we can do on the hardware side until everything arrives.

## 4/18

###### Objectives:
- Assemble control unit PCB

All the PCB boards and electrical components finally arrived. I was able to bring everything together and start working on assembling the boards. This was the first time we were actually able to build and test our hardware since the first two iterations of our design had to be scrapped. I began by working on the control unit. This took a few hours as I had to solder on the resistors and capacitors first, followed by the voltage regulator, SD card slot, ESP32 WROOM, headers, and some loose wires for interfacing later. It took some time as many of those connections, such as the ESP32 WROOM and SD card, are very small and precise. I'm not sure if this is moving fast enough, but there was a lot of progress made today.

![](controlunit.png)
##### Asembled Control Unit

## 4/20

###### Objectives:
- Assemble IMU PCB

Now that the control unit has been constructed, I had to work on start the IMU units. Seeing how long it took to build the control unit, it should take a little less time, but I'm not sure if we will have more than one IMU node for the mock demo. However, my goal is currently to just get the first one done so that we can test data acquisition between the sensor chip, microcontroller, and the SD card.

One of the interesting parts of this assembly was putting on the IMU sensor. The ISM330DHCXTR is a DFN package (I believe), so it only has connections on the bottom of the chip. This means that none of the pads or connections are exposed after it has been soldered on. To solder it, I needed to put some solder flux over the IMU pads on the PCB, and then melted small traces of solder onto the individual pads. I then replaced the flux in order to put the actual chip on. To perform this, I needed to use the heat gun to "heat soak" the chip and solder, and then wait for the heat of the chip and air to get high enough, where the solder would melt. Once the solder is liquified and makes contact with the chip, the chip falls is attracted into place by the pads and solder, which is very satisfying to watch. After that, the heat needs to dissipate slowly away as to not heat shock any of the components. It was a very interesting and patient process, and I will need to do it 5 more times.

Lastly, for today, I took the connectors for our 7.4V S LiPo battery, and found a converter. We were able to solder some connections together to make the board connections compatible with the battery. Lionel was also able to finish CAD models and printed out a case for the control unit, and a single IMU unit (as it is subject to change). We were able to put the IMU unit in its case, and then put the control unit and battery in the other case.

![](imu.png)
##### Assembled IMU Node Unit

## 4/22

### Mock Demo

Today was our Mock Demo. While we got a large amount of work done in the past week, there were still many things to continue working on and improve upon. At this point, we were able to describe what work we had done and how far we had gotten overall, but we were not able to show anything remotely similar to what we were hoping for as a demonstration. I was able to let Zhicong take a look at the completed control unit, along with one of the IMU units. While they were assembled individually, they had not been wired together yet to run data collection tests. At this point, I have been able to perform simple power tests on both units. Stephanie was able to get some tracking software using a depth camera and demonstrating that our camera-tracker was functional. We had hoped to show data collection and full voltage regulation, but it just did not work out in the end.

## 4/25

### Last stretch of assembly

###### Objectives:
- Finish PCB assembly

So far, I have put together the control unit and one of the IMU nodes. I currently have to work on assembling the other 5 IMU units, along with the wiring that will connect them all together. Today, I was able to solder on the basic components onto 2 of the 5 remaining IMU PCB boards. This means that all the resistors, capacitors, and headers are attached to two of the boards. It took a lot longer than I had anticipated though, so I will return to assemble the last three tomorrow. Since I have experienced it now, the rest of the process should be a little easier and quicker.

## 4/26
I came back to the lab to work on the last three PCB boards. Same process as yesterday, but it was easier since I was able to finish half of them already. This took about as long as it did for the other two but it was much easier since it was just a repeat. I also put all of the IMU chips onto the PCB IMU nodes using the heat gun again. In terms of all the PCB boards, everything is put on and assembled except for some diodes and the LEDs, which are only necessary for testing purposes (The LED shows that power is able to run through the board).

While in between, we realized that because we had failed some of our 3D case prints, we did not have enough TPU material left to finish the last two IMU cases, so we would only have 4 IMUs shown in the demo as for safety concerns of exposed wiring.

I spent a lot of time working on the wiring to connect the IMU nodes to the control unit. This was really tedious as we needed lengths long enough to reach to each of the body parts, as well as custom crimping the wires to fit into the headers for each connection joint. I got very crafty with my techniques as I went from having a major connection joint soldered from the control unit, to putting multiple wires in a single crimp. Overall, I think I crimped almost 50 wire ends just to make 4 IMUs connect, which was a lot of time spent and very frustrating. I also had to make some 3-way hook joints (make hook shapes out of the wire ends and then solder them together) that stemmed from the control unit). For the four IMU units that we were able to put in cases, I threaded a single wire from each of them all the way back to the control unit as an output signal wire. This whole process was very tiresome and brutal. I think I literally lost a night of sleep working on this.

## 4/28

### Final Demo

Today, we had our final demo, which we presented to both Prof. Song and our TA Zhicong. We met about an hour and a half before the demo to run through everything again, and make sure that we could show everything that we were able to finish. Basically, we just wired all our 4 IMU units to the control unit, and strapped the tracking suit to Steph. We also retested the skeleton wireframe projection and set it up in a more open area for the demo. Overall, we did not accomplish nearly as much as we had wanted to, but we still had a lot of work done, so we were still able to present something. In terms of R&V tables, we were able to show a pressure/weight test of our TPU cases, some of the voltage tables (the voltage ran through to the other components, but was not regulated), and part of the camera software that was able to display 2D wireframe projection. We really had hoped to have had more success throughout the project, but while there were many setbacks and delays, many of them we could not account for (shipping delays, sensors running out of stock, personal injuries/conflicting schedules).

Since our final demo presentation was a lot later in the week, we also had to work on preparing for our mock presentation. We took a template and covered all the important parts of our project, overviewing the project in its current state and its results. This included things such as our block diagram, PCB and hardware design (along with schematics, and important components), and the software tracking system that we ended up changing towards the latter end of the project. We are still working on it, and probably will not have it completely finished by the time of the mock presentation, so we are mainly writing down the larger points and main ideas, and will later proceed with descriptions after the mock demo.

## 4/29

### Mock Presentation

We arrived to ECEB, ready to present our still-in-progress presentation. Another team presented before us and we were able to get a gist on a way to layout our presentation. Our presentation was 90% enough to show, but it was helpful to see how another group went into their project work with their highlights and overview. Obviously, we presented after them to two TAs and a literary analyst (something along those lines, I think) who was able to give a more outside look, not knowing what our project was about, and also providing tips and suggestions on ways to improve our presentation.

These are some of the tips that we were encouraged to take into suggestion:
- distinguish pictures
- label pictures and make font readable for the someone further from the board/screen
- compress long descriptions into bullet points
- avoid words such as "kinda, gonna, wanna"
- make schematic pictures bigger so that the audience can see the smaller components
- make mechanical design slide pictures contrast the background, hard to see
- R&V tables: make into bullet points
- do not list verification results next to R&V table; feels disappointing
- enunciate any important numbers and terms
- specify important details, i.e. if a component was "shorted" or a line of code did not work
- capitalize the first world of each new bullet point
- if not talking, try not to be distracting (hand movements, swaying body)
- add more notes about our successes within our project

Honestly, these are a lot of useful notes and suggestions for both the presentation itself and the way we performed. I can get very self-conscious about how I am during a presentation, so I personally really enjoyed this feedback because it will help me feel more confident and composed once the final presentation rolls around on Monday. Our goal right now is to edit our presentation with these suggestions and add on to some of the slides and graphics that were lacking during the mock.


## 4/30

Although it wasn't necessary, we came back together to work on our project again. Right now, we think our biggest issue is that the voltage regulator was shorted out, so the voltages and currents were not operating at optimal values, causing components to overheat and perform poorly. We were able to desolder the voltage regulator and remove the burnt one, replacing it with a fresh one. However, after performing this, the current wasn't running throughout the board, so there must have been a short somewhere else on the board, which is really unfortunate. It would have been nice to get something working again on the hardware side, but unfortunately, we were not able to diagnose and figure out what went wrong. Since we could not get our hardware to work again, we turned our focus back to the presentation. Since that is the only thing we have left, it is not worth it to keep troubleshooting our project when it might not have the potential to be fully functional by the time of the presentation.

## 5/1

Steph and Lionel were able to meet and take pictures of the whole hardware system, as well as some example and demo photos of the software tracking in action. I was not able to meet with them in person as I had a time conflict, but we were able to meet via voice chat on discord where we overlooked our slides and presentation. We polished up our slides, added videos and pictures, and split up our presentation into sections we each individually worked on the most (what we had most expertise on). I believe we are ready to present.

## 5/2

### Final Presentation

Steph, Lionel, and I came in for our final presentation with Zhicong and Prof. Song. Although in the end, many of our things didn't work. But we were able to describe our project in detail, including many of the reasons why we fell short, and how we fell short. It was a little sad hearing about how through the length of our project time, we were not able to meet all the goals, requirements, and verifications we had originally planned. We understood the concepts and problems of our project, and even though we were not able to make it work in the end, I think it turned out okay for what we had. Of course, I wish things had gone better, but you can only hope sometimes. It's a funny feeling knowing that the majority of this project and class are 99% done. All that's left is the final report and notebook updates.

## 5/4

### Final Report

This is the last day that we have to work on this project together and honestly, it is really strange that we made it this far. I know our team definitely wanted to have more success overall, but we still made a decent amount of progress, even though we were setback and delayed multiple times. For this last session, Lionel and I took a lot of time looking through our design document and updating the subsystems, R&V tables, and descriptions. We were then able to move and integrate them into our final report, adding on the conclusion section, updated images. Steph joined later and was able to help with the formatting of the report.

It's weird knowing that we are finally done with this class, but we have crossed the last stepping stone of this project. I really wish we could have had a lot more progress on this project on many fronts, but in the end, it is what it is. We definitely did have more success than I had expected after each setback occurred, so I'm not entirely disappointed and it's quite nice to know that we didn't completely flunk through this class. I think if we had more time to work on this, we definitely would have been able to achieve so much more overall. We did have a game plan to really work out the functionality of the hardware and the software, but we just weren't able to get there with the time allotted.
