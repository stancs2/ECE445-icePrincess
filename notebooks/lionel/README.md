# Lionel's ECE445 Notebook
 - [2022-02-03](#2022-02-03) 
 - [2022-02-07](#2022-02-07) 
 - [2022-02-08](#2022-02-08)
 - [2022-02-09](#2022-02-09) 
 - [2022-02-10](#2022-02-10)  
 - [2022-02-14](#2022-02-14)   
 - [2022-02-16](#2022-02-16) 
 - [2022-02-20](#2022-02-20) 
 - [2022-02-21](#2022-02-21) 
 - [2022-02-23](#2022-02-23) 
 - [2022-02-24](#2022-02-24) 
 - [2022-02-28](#2022-02-28) 
 - [2022-03-03](#2022-03-03) 
 - [2022-03-07](#2022-03-07) 
 - [2022-03-09](#2022-03-09) 
 - [2022-03-21](#2022-03-21) 
 - [2022-03-23](#2022-03-23) 
 - [2022-03-28](#2022-03-28) 
 - [2022-03-30](#2022-03-30) 
 - [2022-04-03](#2022-04-03) 
 - [2022-04-04](#2022-04-04) 
 - [2022-04-05](#2022-04-05)
 - [2022-04-11](#2022-04-11)
 - [2022-04-13](#2022-04-13)
 - [2022-04-20](#2022-04-20)
 - [2022-04-21](#2022-04-21)
 - [2022-04-22](#2022-04-22)
 - [2022-04-24](#2022-04-24)
 - [2022-04-25](#2022-04-25)
 - [2022-04-26](#2022-04-26)
 - [2022-04-27](#2022-04-27)
 - [2022-04-28](#2022-04-28)
 - [2022-04-29](#2022-04-29)
 - [2022-04-30](#2022-04-30)
 - [2022-05-01](#2022-05-01)
 - [2022-05-02](#2022-05-02)
 - [2022-05-03](#2022-05-03)
 - [2022-05-04](#2022-05-04)

## 2022-02-03
We have finalized our group by adding a third member and are setting up an initial meeting.

## 2022-02-07
First meeting between the three of us. We have set up twice-weekly meeting times (MW afternoons) so that we can stay up to date with each others' progress and have time to collaborate.

## 2022-02-08
First meeting with our TA. Mentioned that project would be a difficult undertaking and together we discussed briefly areas which we could simplify the project in order to make sure it gets done on time.

## 2022-02-09
Worked on the project proposal as follows:
- Copied over previous work into new proposal doc.
- Set up title page.
- Expanded the problem statement in the introduction section. 
- Started researching safety factors of the project relating to high voltage/current and potential exposure to project users.

## 2022-02-10
summary of what me and TA (David?) talked about:
- best interface protocol is kind of just dependent on what electronics we pick that can satisfy our requirements, so we should do a little research and pick an IMU and microcontroller for now that have compatible communication protocols.
- he mentioned that using IMUs will likely be a lot more difficult than we anticipate. There's apparently a phenomenon called "drifting" where an IMU left alone, in his example for ~5 minutes, will start rotating its own orientation reading without actually moving. He also drew me a diagram where an IMU is tilted w.r.t. the ground and therefore has a gravity vector pointing out of it. Not entirely sure how this was relevant but he mentioned needing to calculate things like the angle between gravity vector and the board, which is very difficult to do. Gyro data can help make up for stuff like "drift" but doing sensor fusion calculations is also very difficult. So one IMU he recommended looking at is the BNO055 which apparently can do some of this math internally, but doesn't resolve the issue perfectly. I also showed him the IMU that Alan Tokarsky sent and he said he probably wouldn't want to use that one specifically. He also mentioned that it might just be a good idea to manufacture the IMUs ourselves by copying the data sheet schematics, but idk if I'm entirely convinced about that.
- we also talked about how to time the IMU and camera data to sync them up. I brought up the RTC chip which he agreed was one way to do it. from there we kind of identified either the timing issue or the motion tracking system accuracy as an area to focus on for the tolerance analysis section of the proposal. I'm not sure how we can determine acceptable failure tolerances for these things without actually testing a finished product and seeing what works, but I guess that's something we need to estimate now and are somehow required to prove through mathematical analysis or simulation.
- for the ethics section, we probably don't have any ethical concerns, but it's still good to include a blurb about not having any and our reasoning behind that in order to demonstrate that we've considered ethical implications.

Finished up the project proposal. Wrote the safety and ethics section. Created first iteration of requirements for each subsystem.

## 2022-02-14
We wanted to start identifying and sourcing specific electronics components needed for our project. I inquired with the TAs about potentially getting recommendations or a suggested list of possible microcontrollers to use, but did not hear anything back. The main issue with selecting a microcontroller is that most groups seem to use chips with wifi capabilities or other features that are not needed in our design. We would like to avoid this in order to reduce the cost and complexity that comes with using a microcontroller, but we cannot find anything that suits these desires. We are choosing the ESP32 WROOM as the placeholder microcontroller currently. 

Our current parts list (subject to change) as follows:
- BNO055 Sensor - x1 for each sensor unit, x6 total currently)
- microcontroller (ESP 32 WROOM) - for main central unit
- ribbon cables
- 2S LiPo Battery (7.4 V)
- MicroSD breakout board
- camera (GoPro, 30 FPS)
- RTC (DS1307 - Real-Time Clock Chip, i2c connection)
- resistors
- capacitors
- voltage regulator (LM1117)
- Thermoplastic Polyurethane (TPU) -> 3D printing casings
- mounting screws
- LEDs

## 2022-02-16
Team meeting to start planning for writing the design document. Created a new template and transferred over material from the project proposal. Outlined a TODO list for all the sections that we need to start writing. Also looked more at sourcing our proposed electronics parts and checked ECE supply center inventory to see which things we would be able to get for free.

## 2022-02-20
Worked on preparing the design document for our design document check. Formalized the R&V tables for all subsystems based on ideas developed during the project proposal. Reworked tolerance analysis section to discuss issues with battery life of the wearable electroincs system. We identified this as an important concern because battery operation conditions are not ideal in the cold weather of an ice rink and we want to consider the commercial applications of the necessary frequency of battery charging. We determined that the battery needs to last multiple hours so that it does not need to be replaced every single coaching session and cause wasted time and frustration for the product users.

## 2022-02-21
Attended design document check with Prof. Schuh, did not get roasted nearly as badly as expected. I take it as a sign that we are making more progress than I had initially anticipated compared to other groups, and we received some very solid feedback that we will try to incorporate by the DD deadline on Thursday.

## 2022-02-23
Met with the team to discuss implementation of changes mentioned in the design document check. My sections of focus would be fleshing out the R&V tables, revising the subsystem descriptions, and updating the safety & ethics section. As a team, we also reviewed our high-level requirements and made some slight modifications as requested by Prof. Schuh.

## 2022-02-24
We finished the design document and submitted it before the deadline. I completed all of my assignments (as mentioned in the previous day's post) and formatted our final submission from Google Doc into the word document. Steph worked on the visuals as well as cost & scheduling, while Ethan worked on the schematics and the tolerance analysis. 

## 2022-02-28
Attended our design review with Prof. Song. He was very interested in the product as he could envision using it himself for golf and saw a lot of potential in terms of marketability for a lower cost product compared to what's on the market now. I think the general feeling was that our project is good, but potentially may be too overambitious for the time and design limitations we have to complete it. He suggested coming up with a backup solution in which we analyze the camera feed frame by frame, so that we are working with still images instead of a video. This could simplify the project enough in case we encounter difficulties making the video feed work.

## 2022-03-03
Team meeting to discuss the feedback received at the board review. We also went over what our objectives and deliverables would be for the next week leading up to spring break.

## 2022-03-07
Another short team meeting talking about similar things as last week. Checked in on Ethan's progress towards completing the PCBs.

## 2022-03-09
Last team meeting before spring break. Discussed goals for the break and what we would start doing once we got back. Started looking at what parts to order and thinking about how to 3D print the protective cases.

## 2022-03-21
Met with Ethan and found replacement voltage regulators for the PCBs, also determined which battery to buy for optimal balance of size, charge capacity, and cost. Ordered all parts through the ECE department. 

## 2022-03-23
Team meeting to try and find a replacement for the BHI260AB IMU sensor, which went out of stock the day after we submitted the purchase order for it. Looked through practically all of the Bosch family of IMUs and everything is out of stock or discontinued. Sent in a quote request for the BMI055 on a long shot, but will look for alternatives. We started looking at other brands of sensors as well, but there doesn't seem to be anything else that has sensor fusion integrated. Top prospect is the ISM330DHCXTR by STMicroelectronics (https://estore.st.com/en/ism330dhcxtr-cpn.html).

## 2022-03-28
No response on the quote request yet, so we decided to just go with the ISM sensor for Ethan to be able to re-design the IMU PCB in time for the second round of orders. We started looking into some sensor fusion algorithms that we can apply using Python. This repo seems to have something interesting that we would be able to integrate: https://github.com/morgil/madgwick_py.

## 2022-03-30
Wrote individual report detailing my work on the project so far and what I still plan to do.

## 2022-04-03
Starting working on OpenPose setup on my laptop. In order to modify the existing code for our project, I would need to compile and run the OpenPose code from Source. This requires me to delete Anaconda (interferes with the program somehow) and download a slew of other prerequisite software. I attenmpted this, but I am having issues with things like cudNN because the installation guides do not match up with how my system is set up and what is being displayed on my terminal.

## 2022-04-04
Pivoted to using the Windows portable demo of OpenPose which requires significantly less setup work. I did run into an issue with getting OpenPose set up properly, but I found documentation addressing this issue and will try to implement their recommended fix. If it doesn't work, I might have to consider pivoting to a Linux system which seems to have more support and less hassle for using OpenPose. However this would raise new concerns about how to acquire a Linux system and the impact this would have on being able to interface with other aspects of the project (both hardware and software) as well as the theoretical commercial feasibilty of requiring our project to be deployed on Ubuntu, but this is less of a concern that being able to get the project working at all.

## 2022-04-05
Brought up two ideas to Zhicong, using a Linux VM and creating a separate Windows user to try and bypass the issues with software being pre-downloaded. Need to find a way to get a clean slate. Linux VM would allow for better support as there are more detailed and better tailored instructions for that OS. 

## 2022-04-11
Tried setting up the prerequisite programs/libraries on Linux VM but ran into issues seemingly with how the VM recognized my laptop's graphics card. Specifically ran into issues with setting up CUDA, could not resolve the error messages and it seems that using a VM (or at least this VMWare machine specifically) is a bust. Also realized separate Windows user was a dumb idea since multiple users still seem to share the same program files, so that would not actually allow me to have a clean slate with a different windows user.

## 2022-04-13
Talked to Zhicong again and he suggested using WSL, built-in Linux environment for Windows OS. Looked into WSL differences to VM and how it could potentially affect the project, but the discussion was at a technical level above my experience. Tried installing and ran into issues surrounding this error, "virtual hard disk files must be uncompressed and unencrypted and must not be sparse." Continual issues with setting up this software on my laptop are not seeming to let up, so I'm wondering if we should try and do this on Steph's desktop which is more powerful and has less dependancies already installed. Would provide for a cleaner slate than my laptop, but raises the concerns of mobility for the actual project demo. Alternatively, we may be pivoting to using Xbox Kinect camera and some compatible libraries to do skeleton processing instead. Steph mentioned something about her brother shipping the Kinect camera from home.

Also started working on the CAD for the protective cases. Designing with a general internal structure in mind and a rudimentary latching system for the control unit covers, but will definitely need to revise some dimensions and details once I get more information from Ethan about certain PCB dimensions. Need to add mounting holes and gaps in the case walls for outgoing wires.

## 2022-04-20
Team meeting at the ESPL, downloaded Processing IDE and installed j4k library into proper location. Explored the j4k library (https://research.dwi.ufl.edu/projects/ufdw/j4k/index.php) and tried to understand how it is used, the potential capabilities that it would allow us, and how exactly it interfaces with the IDEs like Processing and Eclipse. This shift does not entirely suit me personally as I have not had experience in Java programming or using these IDEs since high school, but Steph should be more familiar than I am, and I'm certainly willing to explore this option if we're able to get it working. 

## 2022-04-21
Grabbed some necessary hardware components including ribbon cable and brought it to Ethan in the lab. Worked alongside him and procured the last needed dimensions for the CAD files. Made some progress on those but will probably try and finish up the completed first iterations tomorrow so that we can start mass printing them very soon. Also trying to get my roommate to help set up the 3D printer in our apartment so that we can get the cases printing as soon as the design is finished.

## 2022-04-22
Attended mock demo with Zhicong. Not much to be able to show except for work being done on the PCBs. Discussed an issue with him regarding some pads on the IMU PCB being too close together and causing a short on the IMU chip. We talked about taking an exacto knife and cutting the pads up, but upon further discussion it seems that this will not be an issue which affects the functionality of the IMUs. Will need to do a lot of catching up over the weekend to prepare for demo in time. Some progress made with the wireframe projection capabilities of the Xbox Kinect camera but we are continuing to develop the software as well. 

## 2022-04-24
Finished up CAD and printed the control unit as well as an IMU box. Will fit the electronics into both of these and see if any dimension changes are needed for another iteration. Printing has been a bit harder than expected as 3D printers generally do not work well with such soft filaments like TPU. I had to do some settings testing and luckily found a pretty good balance while still being able to print at much faster speeds than typically recommended. I started encountering some print failures with the IMU cases and after a couple tries I realized this was due to the bottom print layers not adhering to the bed and the corners starting warping upwards. This eventually caught onto the nozzle of the extruder which ripped the entire print off the bed and turned it into spaghetti. I looked up some fixes and got some water soluble glue sticks as well as painters tape to increase adhesion to the bed. I tried the glue first and it worked extremely well so I think this issue is now resolved.

(later in the day) Picked up more mounting hardware and other parts and then brought to Ethan in the lab. We tested the fits for the PCBs in the cases and it looks like I got it exactly right on the first try, which is kinda lucky but I'll definitely take it. Will continue to print out more cases over the next couple days now that this is confirmed.

## 2022-04-25
First full team meeting in a bit. Steph provided some feedback on the exterior design and look of the protective cases, which I incorporated into a revised version 2 and then version 3 of the IMU box. The new designs are slightly more compact and have a more aerodynamic feel for a closer look to a real "market" product.

CAD screenshots of the final versions for each case can be found in this repo folder.

## 2022-04-26
Worked at Steph's with her on the code. Got body measurements from her so that Ethan could cut wire lengths to run between PCBs and best match her dimensions. Tried to get Eclipse set up for working with the j4k library as Steph seemed to be unable to use Processing with it. Bigger issue seems to be that the full functionality of the library can only be unlocked by setting up in Enclipse. Ethan came by to pick up more printed cases as well as mounting stuff and a microcontroller programmer we got from my roommate. We went back to the lab later and tried to work on programming the microcontroller, but encountered difficulties connecting to it with the programmer. We also noticed some kind of power issue or short, as the microcontroller is getting extremely hot beyond the point of being able to touch it once we power it through the programmer. There also seems to be a separate issue with the power regulation subsystem as the voltage regulator is not working and the battery is delivering the full 7.4V to the entire control and IMU system. I don't think any of the components are rated for continuous voltages this high, so this is definitely a problem for being able to test the project. 

Side note, the new versions of the IMU cases were overcompacted to the point where the covers no longer fit over the electronics. There was a wire connector dimension which had not been added to the PCB at the time that juts up too high past the cover slots. We also ran out of filament after printing four of the six planned IMU cases, so we with that in mind and in the interest of time, we are decreasing the number of nodes to four for the current project. We re-examined the wearable system and determined that the least impactful joints which we could remove were the hip and elbow sensors. 

## 2022-04-27
Worked on trying to set up OpenCV object tracking with the specific Logitech webcam (C920) that was borrowed from my roommate. Setting up the environment was going pretty well until I ran into some issues with the code being unable to find certain filepaths. Left this up to Steph to continue diagnosing and later went into lab with Ethan and kept working on microcontroller programming. Found and incorporated Arduino code to test all necessary components (SD card, RTC chip, and IMU sensors) but unable to actually apply this due to the programmer issues discovered yesterday. 

## 2022-04-28
Demo and prepared for mock presentation. Stress tests were conducted on the protective cases using 110 lbs of instantaneous force and they passed with flying colors. Seems like this initial estimate is undervaluing the strength of our case structure and could be ramped up to accomodate for higher weights. Demo'd what we had (mechanically complete electronics system and wireframe projection software) on Zoom to the Professor, Zhicong, and another TA. Later in the day regrouped and created the presentation for mock presentation tomorrow. Used the Grainger Engineering presentation template and created the general format for our presentation. Added RV tables and results sections for most of the subsystems. Added some suggestions for future works.

## 2022-04-29
Mock presentation with some TAs. Received good feedback on the content and format of our presentation as well as some notes on presentation behaviors and cutting down on fidgeting and filler words. Will take this advice into account as we refine the presentation slides leading up to our final presentation on Monday.

## 2022-04-30
Attempted to resolder voltage regulator, ended up shorting the entire board where nothing powers up anymore. Set up ESP32 dev board in last effort attempt to program IMUs individually, but we encountered compile errors that were not resolved. This was an unexpected roadblock as the IMU code we adapted was from the chip manufacturers directly. 

## 2022-05-01
Took photos and videos for presentation and finished working on the slides. I refined the delivery of the R&V tables/results sections for the subsystems. I also felt like it would be beneficial to the flow of our presentation to add a small design intro section to each subsystem so that we could in some detail describe the idea of the original design for each system and then more immediately contrast it with our accomplishments and revisions, as opposed to describing all of the original design at the beginning of the presentation where it would be more easy for the viewer to forget some of the details.

## 2022-05-02
Got up early to do project presentation prep. Finalized all last minute changes to the slides and divided up our slides and practiced our delivery of information a couple times. Presented to Zhicong and the Professor.

## 2022-05-03
Started working on the final paper. Imported previous design document to new doc and made some updates to cover page and our objectives list. Mostly just looked at the guidelines and assignment page for the final report to create a new objectives list for everything that we needed to update, scrap, or add to our existing work.


## 2022-05-04
Finished writing the final paper. Wrote the abstract, verification sections for IMU and software, and some sections of the conclusion. Also made edits as necessary to the subsystem design descriptions depending on what changes we had to make to our design over the course of the semester. Some big changes were made with the IMU nodes and camera & software as things did not go to our original plan for multiple reasons previously described in the notebook. Lastly, I copied all of our work over to a new Word template as well as added new images and updated figure/table descriptions to comply with the guideline requirements. 

## 2022-05-05
Went into lab and did the lab checkout. Uploaded photos of the project and our presentation slides. Also deleted some old duplicate files. Lastly updated this notebook for the final time. GG ECE 445.
