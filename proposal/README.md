# ICE PRINCESS

Team Members:
- Stephanie Tancs (stancs2)
- Lionel Binder (lbinder2)
- Ethan Yee (ethanay2)

# Contents
- [1. Introduction](#1-introduction) 
  - [1.1 Problem](#11-problem)
  - [1.2 Solution](#12-solution)
  - [1.3 Visual Aid](#13-visual-aid)
  - [1.4 High-Level Requirements](#14-high-level-requirements)
- [2. Design](#2-design)
  - [2.1 Block Diagram](#21-block-diagram)
  - [2.2 Subsystems](#22-subsystems)
    - [2.2.1 Power Regulation](#221-power-regulation)
    - [2.2.2 IMU Nodes](#222-imu-nodes)
    - [2.2.3 Microcontroller](#223-microcontroller)
    - [2.2.4 Camera and Software](#224-camera-and-software)
  - [2.3 Tolerance Analysis](#23-tolerance-analysis)
- [3. Ethics and Safety](#3-ethics-and-safety)
- [References](#references)

# 1. Introduction

## 1.1 Problem

Coaching for figure skating costs $50+ / 25 min of instruction. With the costs of ice time and skate maintenance outside of this, the cost of figure skating is significant if you are looking to improve your skills in any way. This makes it impossible for many who would like to improve or begin figure skating to do so based solely on cost. For two years of twice-weekly coaching, the cost would be $10,400 for coaching alone. Our solution attempts to reduce this barrier to entry.

Aside from the cost, ice rinks are not always easily accessible physically, especially in more rural areas where people have to travel farther to reach their nearest rink. Enabling the possibility for a remote coaching element in our solution could connect a higher number and quality of coaches to areas that are underserved in that regard and increase popularity of the sport. Moreover, some students are unable to find coaches that teach with respect to their style of learning. Those who are visual learners struggle to find coaches who can explain complex movements in a way they can understand. This only increases the struggle of understanding what they are doing wrong when they reach a point of confusion or misunderstanding. 

## 1.2 Solution

With a system consisting of wearable electronics, a camera setup, and access to a computer, each ice rink could be equipped for digital coaching and encourage skaters to improve without needing to invest significant amounts of money into coaching. The system would function as the wearable electronics can record accelerations in 3 dimensions, and the software can synthesize this information and the input from the camera in order to create a model of the skater and compare it directly with an ideal model. This is so the skater can directly see what they are attempting and make specific changes to their motion in order to perfect their form. Furthermore, this solution would aid the coaching of those who have a different style of learning (visual, tactile) as opposed to auditory to understand the corrections made.

## 1.3 Visual Aid
![](visualAid.png)

## 1.4 High-Level Requirements

This project can be demonstrated in the UI Ice Arena with one of the group members (Stephanie) who is a well-trained and experienced figure skater. This demonstration can be easily video recorded and the data input from the video and system can be processed, while the coded program itself can be run in class.
Our high-level goals are as follows:
- Design and build a wearable electronics system to measure the acceleration data of an ice skater performing the Biellmann (a skating skill in which the skater lifts the leg above their head and grabs onto the blade with their hands)
- Utilize camera input to align with acceleration data and create a full depiction of the skater in terms of kinematics
- Use aggregated data to generate a visualization of the skater and quantify the difference between an “ideal” move versus the skater’s move

# 2. Design

## 2.1 Block Diagram
![](blockDiagram.png)

## 2.2 Subsystems

### 2.2.1 Power Regulation

Our wearable design needs to consistently power each IMU node strapped to the body. The main voltage provided will be set to 7.4V to provide efficient power and will use a voltage regulator to maintain stable power through multiple IMUs. While each IMU sensor could be powered individually, they will all be connected by ribbon cable to a central node, so they can share the same power source to model a more reliable system.
- Requirement 1: The power regulation subsystem must be able to supply at least 300mA to the rest of the system continuously at 3.3V ± 0.1V.

### 2.2.2 IMU nodes

We will manufacture multiple IMU nodes consisting of an accelerometer and gyroscope to be outfitted on an individual at important body joints. These nodes will be able to record acceleration data from the wearer and subsequently transmit through a wired connection to the master microcontroller node that is collecting data. The IMUs make up one half of the wearable electronics. In order to attach these nodes to the skaters, we will CAD and 3D print a protective case, and pin the electronics onto the skater via a safety pin to ensure maintenance of a precise location. The node on the skate will be attached via a safety pin to a sleeve (commonly sold as a skate protector to freestyle skaters) that goes over the boot of the skate. We plan to use the BNO055 sensor for our IMU nodes.
- Requirement 1: Record acceleration data in the x,y,z plane within 15% accuracy.
- Requirement 2: Transmit acceleration data to the microcontroller.

### 2.2.3 Microcontroller

We will also manufacture a custom microcontroller that serves as the master node for the wearable electronics system. This microcontroller will need to be wired to all of the IMU nodes in order to receive and aggregate the wearer’s acceleration data. It will also require a storage system, such as an SD card, in order to later transfer the accumulated data to the software component of the project.  It is easiest to use compatible IMUs which operate on the same communication protocol as the microcontroller, allowing an easier transaction of information. We will also need to incorporate an RTC in order to later sync the data accurately with the camera recording. The microcontroller makes up the second half of the wearable electronics.
- Requirement 1: Use compatible interface protocols to communicate with the SD card, IMU nodes, and RTC.
- Requirement 2: Have a sufficient number of data pins to accumulate readings from up to eight IMU nodes.

### 2.2.4 Camera and Software

We will need to connect a camera to a computer in order to accomplish the computer vision aspect of this project. The camera will capture the motion of the skater as well as track the world position of the IMU nodes to provide visual data. This will be paired with the IMU data to generate a model of the skater’s movement as they execute skill moves. We can compare these models with “ideal” models based on skilled figure skaters to both quantify the difference between a performed move versus an “ideal” move and to provide visual feedback to the skater as to how they can improve their performance. We plan to use OpenCV or some similar library to accomplish this.
- Requirement 1: Combine hardware output with camera output to create a full quantitative depiction of the skater.
- Requirement 2: Track the nodes on the skater using OpenCV.

## 2.3 Tolerance Analysis

The main idea of our project design involves tracking the movements of the skater who is practicing. The tracking works by locating the orientation and positioning of each IMU node through a set timeline. A potential issue that could pose a problem is not accounting for different body types. For example, a taller person would have longer arms and legs than someone shorter, so the distance between each IMU node would be further. If our base model is based on a 6-foot tall person, but the user is much taller or shorter, the program would have trouble computing a virtual comparison between the user and model. A feasible solution would be to scale either the input or model data to allow an accurate analysis between someone’s current progress and the “ideal” positioning. Understanding how to scale and account for these differences is necessary in order to allow our project to be universally compatible to anyone who uses it.

# 3. Ethics and Safety

We do not believe that there are any serious ethical concerns associated with our project. Neither the wearable electronics system nor the camera and data analysis components could be altered or misused in a way that would pose a threat to the safety or welfare of the users [1]. The only potential issue would be in data privacy of the user, but we feel that there is negligible risk of abuse. The camera recordings of the user would be filmed publically in an ice rink, where other people would be present and there is minimal expectation of privacy. The video would also be stored locally on a laptop, so it could be immediately deleted after the coaching session if the user felt uncomfortable leaving a video record of themselves. Furthermore, there is no ethical risk in the development of our product as human testing will be limited to the members of the group.

We have however identified some safety concerns relating to our project. There is an inherent bodily risk associated with participation in ice skating. To address this, we are exclusively having Stephanie test the system. She has skated for many years and is on the University’s synchronized skating team, so she possesses the experience and ability to safely ice skate. She is also familiar with the specific skill move that we aim to demo, so we are not putting her in increased danger of injury. In the case that an injury does occur, Stephanie has already signed a liability waiver with the ice rink that covers incidents on ice. The other area of safety to address is the wearable electronics system. Because these electronics will be in close proximity to the human body, we incorporated multiple features into our design that will ensure the safety of the user. We plan to power the system with a 2S LiPo battery which will deliver currents low enough to prevent shocking a person. We also plan to insulate all wiring between components as well as encase IMUs and the microcontroller in 3D printed boxes. With the addition of the clothes worn by the user, these layers of protection will keep the user from coming in contact with any current, barring extreme mechanical failure. 

### References


[1] IEEE, “IEEE Code of Ethics.” [Online]. Available: https://www.ieee.org/about/corporate/
governance/p7-8.html

[2] Bosch, “BNO055 Intelligent 9-axis absolute orientation sensor.” [Online]. BST-BNO055-DS000-12, Nov. 2014.

[3] Espressif Systems, “ESP32-SOLO-1.” [Online]. June 2018 [Revised Feb. 2021].
